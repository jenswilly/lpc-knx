 #include "board.h"
 #include "string.h"

// http://nxpblmcusw.com:8080/lpcconfig/lpcconfig
void IOCON_Init()
{
	// Enable IOCON and GPIO clocks
	Chip_Clock_EnablePeriphClock( SYSCTL_CLOCK_IOCON );
	Chip_Clock_EnablePeriphClock( SYSCTL_CLOCK_GPIO );

	/* Pin I/O Configuration */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 0, 0x90); */	// SWD #RESET
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 1, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 2, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 3, 0x81 ); */			/// PIO0_3: USB_VBUS
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 4, 0x1);	*/// I2C SCL
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 5, 0x1);	*/// I2C SDA
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 6, 0x82); */// SPI SCK
	Chip_GPIO_SetPinDIRInput( LPC_GPIO, 0, 7 );			// PIO0_7: input
	Chip_IOCON_PinMuxSet( LPC_IOCON, 0, 7, 0x90 );			// GPIO, p-u: push-button input

	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 8, 0x81 );	*/		// SPI MISO
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 9, 0x81 );	*/// SPI MOSI
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 10, 0x90); */// SWD SWCLK
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 11, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 12, 0x90); */
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 13, (IOCON_FUNC4 | IOCON_MODE_INACT | IOCON_DIGMODE_EN));	// U1_RXD, pin 32
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 14, (IOCON_FUNC4 | IOCON_MODE_INACT | IOCON_DIGMODE_EN));	// U1_TXD, pin 33

	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 15, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 16, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 17, 0x90); */

	Chip_IOCON_PinMuxSet( LPC_IOCON, 0, 18, 0x80 );			// GPIO: LED1
	Chip_IOCON_PinMuxSet( LPC_IOCON, 0, 19, 0x80 );			// GPIO: LED0

	Chip_IOCON_PinMuxSet( LPC_IOCON, 0, 20, 0x90 ); 		// GPIO, p-u: OLED CS#
	Chip_GPIO_SetPinDIROutput( LPC_GPIO, 0, 20 );			// PIO0_20: output
	Chip_GPIO_SetPinState( LPC_GPIO, 0, 20, true );		// high

	Chip_GPIO_SetPinDIROutput( LPC_GPIO, 0, 21 );			// PIO0_21: output
	Chip_IOCON_PinMuxSet( LPC_IOCON, 0, 21, 0x90 );			// GPIO, p-u: OLED D/C#

	Chip_GPIO_SetPinDIROutput( LPC_GPIO, 0, 22 );			// PIO0_22: output
	Chip_IOCON_PinMuxSet( LPC_IOCON, 0, 22, 0x90 ); 		// GPIO, p-u: OLED RST#

	Chip_GPIO_SetPinDIROutput( LPC_GPIO, 0, 23 );		// PIO0_23: output
	Chip_IOCON_PinMuxSet( LPC_IOCON, 0, 23, 0x80 ); 		// GPIO, p-u: SD card CS#
	Chip_GPIO_SetPinState( LPC_GPIO, 0, 23, true );		// high

	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 13, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 20, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 21, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 23, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 24, 0x90); */
	Chip_IOCON_PinMuxSet( LPC_IOCON, 2, 0, 0x1 );			// XTALIN
	Chip_IOCON_PinMuxSet( LPC_IOCON, 2, 1, 0x1 );			// XTALOUT
	Chip_IOCON_PinMuxSet( LPC_IOCON, 2, 2, 0x90 );			// PIO2_2: input, p-u, SC card detect (HIGH (open) means card present)
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 2, 5, 0x90); */
	/*Chip_IOCON_PinMuxSet(LPC_IOCON, 2, 7, 0x90); */
}

/* Set up and initialize clocking prior to call to main */
void Board_SetupClocking(void)
{
	Chip_SetupXtalClocking();
}

/* Set up and initialize hardware prior to call to main */
void Board_SystemInit(void)
{
	/* Setup system clocking and muxing */
	//Board_SetupMuxing();/* Muxing first as it sets up ext oscillator pins */
	IOCON_Init();
	Board_SetupClocking();
}
