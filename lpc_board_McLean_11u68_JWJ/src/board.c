/*
 * @brief NXP LPCXpresso 11U68 board file
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "board.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/* System oscillator rate and RTC oscillator rate */
const uint32_t OscRateIn = 12000000;
const uint32_t RTCOscRateIn = 32768;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Initialize debug output via UART for board */
void Board_Debug_Init(void)
{
#if defined(DEBUG_UART)
	Chip_UART0_Init(DEBUG_UART);
	Chip_UART0_SetBaud(DEBUG_UART, 115200);
	Chip_UART0_ConfigData(DEBUG_UART, (UART0_LCR_WLEN8 | UART0_LCR_SBS_1BIT));
	Chip_UART0_SetupFIFOS(DEBUG_UART, (UART0_FCR_FIFO_EN | UART0_FCR_TRG_LEV2));
	Chip_UART0_TXEnable(DEBUG_UART);
#endif
}


/*****************************************************************************
 * OLED functions
 ****************************************************************************/

#define OLED_DC_PORT 0
#define OLED_DC_PIN 21

#define OLED_RST_PORT 0
#define OLED_RST_PIN 22

/**
 * Sets the state of the D/C# pin.
 * PINMUX and direction should already be set.
 *
 * @param state Set to "true" to set the pin high (for DATA); "false" to set the pin low (for COMMAND)
 */
void Board_OLED_Set_DC( bool state )
{
	Chip_GPIO_SetPinState( LPC_GPIO, OLED_DC_PORT, OLED_DC_PIN, state );
}

/**
 * Sets the state of the RST# pin.
 * PINMUX and direction should already be set.
 *
 * @param state Set to "true" for high; "false" for low
 */
void Board_OLED_Set_RST( bool state )
{
	Chip_GPIO_SetPinState( LPC_GPIO, OLED_RST_PORT, OLED_RST_PIN, state );
}


/*****************************************************************************
 * SPI CS functions
 ****************************************************************************/

#define NUM_SPI_DEVICES 2
// Indices defined in board_api.h: SD, OLED

static const uint8_t spi_cs_ports[ NUM_SPI_DEVICES ] = {0, 0};
static const uint8_t spi_cs_pins[ NUM_SPI_DEVICES ] = {23, 20};

/**
 * Assert/deassert SSEL pin for a SPI device.
 *
 * @param device		Index of the device in the spi_cs_ports and spi_cs_pins arrays.
 * @param assert		true if CS should be asserted (i.e. driven low); false to deassert
 */
void Board_SPI_SSEL_Assert( uint8_t device, bool assert )
{
	Chip_GPIO_SetPinState( LPC_GPIO, spi_cs_ports[ device ], spi_cs_pins[ device ], !assert );
}

/*****************************************************************************
 * LED functions
 ****************************************************************************/

#define MAXLEDS 2
static const uint8_t led_ports[ MAXLEDS ] = {0, 0};
static const uint8_t led_pins[ MAXLEDS ] = {19, 18};

/**
 * Initializes board LED(s)
 */
void Board_LED_Init( void )
{
	int idx;

	for( idx = 0; idx < MAXLEDS; idx++ )
	{
		// Set the GPIO as output with initial state off (high)
		Chip_GPIO_SetPinDIROutput( LPC_GPIO, led_ports[idx], led_pins[idx] );
		Chip_GPIO_SetPinState( LPC_GPIO, led_ports[idx], led_pins[idx], false );
	}
}

/**
 *  Sets the state of a board LED to on or off
 */
void Board_LED_Set( uint8_t LEDNumber, bool On )
{
	int idx;

	// First all LEDs off
	for( idx = 0; idx < MAXLEDS; idx++ )
		Chip_GPIO_SetPinState( LPC_GPIO, led_ports[ idx ], led_pins[ idx ], false );

	if( On )
		Chip_GPIO_SetPinState( LPC_GPIO, led_ports[ LEDNumber ], led_pins[ LEDNumber ], On );
}

/* Returns the current state of a board LED */
bool Board_LED_Test(uint8_t LEDNumber)
{
	bool state = false;

	if (LEDNumber < MAXLEDS) {
		state = !Chip_GPIO_GetPinState( LPC_GPIO, led_ports[LEDNumber], led_pins[LEDNumber] );
	}

	return state;
}

/* Toggles the current state of a board LED */
void Board_LED_Toggle(uint8_t LEDNumber)
{
	Board_LED_Set( LEDNumber, !Board_LED_Test( LEDNumber ));
}


/*****************************************************************************
 * SD card
 ****************************************************************************/

#define SD_DETECT_PORT 2
#define SD_DETECT_PIN 2

bool Board_SD_Present()
{
	return Chip_GPIO_GetPinState( LPC_GPIO, SD_DETECT_PORT, SD_DETECT_PIN );
}


/*****************************************************************************
 * General functions
 ****************************************************************************/

/**
 * Set up and initialize all required blocks and functions related to the board hardware
 */
void Board_Init(void)
{
	// Initialize GPIO
	// Chip_GPIO_Init( LPC_GPIO ); 	Already done in pinmux init

	// Initialize LEDs
	Board_LED_Init();
}

// Port/pin definition for PB INT# pin. PINMUX this as GPIO input w/ pull-up in board_sysinit.c
#define PB_PININT_PORT 0
#define PB_PININT_PIN 7

/**
 * Initialize pin interrupts.
 */
void Board_Init_PinInterrupts()
{
	// Enable PININT clock
	Chip_Clock_EnablePeriphClock( SYSCTL_CLOCK_PINT );

	// Configure interrupt channel 0 for the push-button INT# pin
	Chip_SYSCTL_SetPinInterrupt( 0, PB_PININT_PORT, PB_PININT_PIN);

	// Configure channel interrupt as edge sensitive, falling edge
	Chip_PININT_ClearIntStatus( LPC_PININT, PININTCH0 );
	Chip_PININT_SetPinModeEdge( LPC_PININT, PININTCH0 );
	Chip_PININT_EnableIntLow( LPC_PININT, PININTCH0 );

	// Enable interrupt. Implement handler in PIN_INT0_IRQHandler
	NVIC_ClearPendingIRQ( PIN_INT0_IRQn );
	NVIC_EnableIRQ( PIN_INT0_IRQn );
}

