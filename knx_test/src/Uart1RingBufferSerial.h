/*
 * Uart0RingBufferSerial.h
 *
 *  Created on: 25 Oct 2019
 *      Author: jenswilly
 */

#ifndef UART1RINGBUFFERSERIAL_H_
#define UART1RINGBUFFERSERIAL_H_

#include "HardwareSerial.h"
#include "chip.h"

// Transmit and receive ring buffer sizes
#define UART_SRB_SIZE 64	// Send
#define UART_RRB_SIZE 64	// Receive


class Uart1RingBufferSerial: public HardwareSerial
{
private:
	static void InitPinMux();

public:
	Uart1RingBufferSerial();
	~Uart1RingBufferSerial();

	int available() override;
	uint8_t read() override;
	uint8_t peek() override;

    void begin( unsigned long baud, e_SerialMode mode ) override;
	void end() override;

    int write( uint8_t data ) override;
    int write( uint8_t *buffer, int size ) override;
};

#endif /* UART1RINGBUFFERSERIAL_H_ */

