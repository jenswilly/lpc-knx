/*
 * systick.c
 *
 *  Created on: 18 Jul 2019
 *      Author: jenswilly
 */

#include "systick.h"
#include "board.h"


/**
 * Waits (blocking) the specified number of ticks.
 *
 * @param ticks SysTicks to wait
 */
void SysTick_delay( uint32_t ticks )
{
	delay_ticks = 0;
	while( delay_ticks < ticks )
		__WFI();
}

/**
 * Initializes the SysTick timer with the specified frequency.
 *
 * @param freq Number of SysTicks per seconds. E.g. 1000 for one tick every millisecond.
 */
void SysTick_init( uint32_t freq )
{
	/* Enable and setup SysTick Timer at the specified frequency */
	SysTick_Config( SystemCoreClock / freq );
}

/**
 * SysTick interrupt handler
 */
void SysTick_Handler(void)
{
	delay_ticks++;
}

