/*
 * systick.h
 *
 *  Created on: 18 Jul 2019
 *      Author: jenswilly
 */

#ifndef DRIVERS_SYSTICK_H_
#define DRIVERS_SYSTICK_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

static volatile uint32_t delay_ticks = 0;

void SysTick_init( uint32_t freq );
void SysTick_delay( uint32_t ticks );
static inline uint32_t ticks() { return delay_ticks; }
static inline void SysTick_reset() { delay_ticks = 0; }

// For Arduino code compatibility
#define millis() ticks()
#define delay(x) SysTick_delay(x)

#ifdef __cplusplus
}
#endif

#endif /* DRIVERS_SYSTICK_H_ */
