/*
 * Uart0RingBufferSerial.cpp
 *
 *  Created on: 25 Oct 2019
 *      Author: jenswilly
 */

#include "Uart0RingBufferSerial.h"
#include "board.h"
#include "chip.h"

static RINGBUFF_T txring, rxring;
static uint8_t rxbuff[ UART_RRB_SIZE ], txbuff[ UART_SRB_SIZE ];

/**
 * @brief	UART interrupt handler using ring buffers
 * @return	Nothing
 */
extern "C" {
void USART0_IRQHandler(void)
{
	// Want to handle any errors? Do it here.

	// Use default ring buffer handler.
	Chip_UART0_IRQRBHandler( LPC_USART0, &rxring, &txring );
}
}

Uart0RingBufferSerial::Uart0RingBufferSerial()
{
	Uart0RingBufferSerial::InitPinMux();
}

Uart0RingBufferSerial::~Uart0RingBufferSerial()
{
	end();
}

void Uart0RingBufferSerial::begin( unsigned long baud, e_SerialMode mode )
{
	// Setup UART
	Chip_UART0_Init( LPC_USART0 );
	Chip_UART0_SetBaud( LPC_USART0, baud );

	// Configure
	switch( mode ) {
	case SERIAL_8E1:
		// 8 data bits, EVEN parity, 1 stop bit
		Chip_UART0_ConfigData( LPC_USART0, (UART0_LCR_WLEN8 | UART0_LCR_PARITY_EN | UART0_LCR_SBS_1BIT | UART0_LCR_PARITY_EVEN) );

	default:
		// Unsupported mode
		break;
	}

	Chip_UART0_SetupFIFOS (LPC_USART0, (UART0_FCR_FIFO_EN | UART0_FCR_TRG_LEV2) );
	Chip_UART0_TXEnable( LPC_USART0 );

	// Initialize ring buffers
	RingBuffer_Init( &rxring, rxbuff, 1, UART_RRB_SIZE );
	RingBuffer_Init( &txring, txbuff, 1, UART_SRB_SIZE );

	// Enable receive data and line status interrupt
	Chip_UART0_IntEnable( LPC_USART0, (UART0_IER_RBRINT | UART0_IER_RLSINT) );

	// Enable UART 0 interrupt
	NVIC_EnableIRQ( USART0_IRQn );
}

void Uart0RingBufferSerial::end()
{
	// Clear buffers
	RingBuffer_Flush( &rxring );
	RingBuffer_Flush( &txring );

	// DeInitialize UART0 peripheral
	NVIC_DisableIRQ( USART0_IRQn );
	Chip_UART0_DeInit( LPC_USART0 );
}

/**
 * Returns the number of available bytes in the RX buffer
 */
int Uart0RingBufferSerial::available()
{
	return RingBuffer_GetCount( &rxring );
}

/**
 * Read one byte from the RX buffer.
 *
 * Check if any data is available first using Uart0RingBufferSerial::available().
 * If this method is called with no data available, 0 is returned which is not correct as 0 might be a valid data byte.
 */
uint8_t Uart0RingBufferSerial::read()
{
	uint8_t data = 0;
	Chip_UART0_ReadRB( LPC_USART0, &rxring, &data, 1 );
	return data;
}

/**
 * Read one byute from the RX buffer _but leave the byte in the buffer_.
 *
 * This function does _not_ check whether any data is available so be sure to use `Uart0RingBufferSerial::available()` first.
 */
uint8_t Uart0RingBufferSerial::peek()
{
	uint8_t data = 0;

	RingBuffer_Peek( &rxring, &data );
	return data;
}

int Uart0RingBufferSerial::write( uint8_t data )
{
	return Chip_UART0_SendRB( LPC_USART0, &txring, (const uint8_t *)&data, 1 );
}

int Uart0RingBufferSerial::write( uint8_t *buffer, int size )
{
	// No need for the default implementation's for loop: write all bytes at once.
	return Chip_UART0_SendRB( LPC_USART0, &txring, (const uint8_t *)buffer, size );
}

/**
 * Initializes PINMUX for USART0.
 *
 * The LPCXpresso 11u68 uses the following pins:
 *
 * RX | PIO1.26 | D5
 * TX | PIO1.27 | D6
 *
 */
void Uart0RingBufferSerial::InitPinMux()
{
#if defined( BOARD_NXP_LPCXPRESSO_11U68 )
	// For both: Func2, no pull up/down,
	Chip_IOCON_PinMuxSet( LPC_IOCON, 1, 26, (IOCON_FUNC2 | IOCON_MODE_INACT ));
	Chip_IOCON_PinMuxSet( LPC_IOCON, 1, 27, (IOCON_FUNC2 | IOCON_MODE_INACT ));
#elif defined( BOARD_NXP_LPCXPRESSO_11U68_JWJ )
	// The proto board uses P0.18 and P0.19 which are configured in board_sysint.c, IOCON_Init() so no configuration here.
#else
	// Only LPCXpresso 11u68 is supported for now
	#error "No UART setup defined"
#endif
}
