/*
 * Uart0RingBufferSerial.h
 *
 *  Created on: 25 Oct 2019
 *      Author: jenswilly
 */

#ifndef UART0RINGBUFFERSERIAL_H_
#define UART0RINGBUFFERSERIAL_H_

#include "HardwareSerial.h"

// Transmit and receive ring buffer sizes
#define UART_SRB_SIZE 64	// Send
#define UART_RRB_SIZE 64	// Receive


class Uart0RingBufferSerial: public HardwareSerial
{
private:
	static void InitPinMux();


public:
	Uart0RingBufferSerial();
	~Uart0RingBufferSerial();

	int available() override;
	uint8_t read() override;
	uint8_t peek() override;

    void begin( unsigned long baud, e_SerialMode mode ) override;
	void end() override;

    int write( uint8_t data ) override;
    int write( uint8_t *buffer, int size ) override;
};

#endif /* UART0RINGBUFFERSERIAL_H_ */

