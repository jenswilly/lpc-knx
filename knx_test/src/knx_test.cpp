/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */


#include "board.h"

/* Define KNX library version
 *
 * 1	KnxDevice from https://github.com/franckmarini/KnxDevice (more advanced requiring microsecond counting)
 * 2	KnxTpUart from https://bitbucket.org/thorstengehrig/arduino-tpuart-knx-user-forum/src/default/ (simpler)
 * 3	Simple reading of 6 bytes header for debugging
 */
#define KNX_LIBRARY 2

#include <cr_section_macros.h>
#include "drivers/systick.h"
#include "Uart0RingBufferSerial.h"
#include "Uart1RingBufferSerial.h"
#if KNX_LIBRARY == 1
#include "KnxDevice/KnxDevice.h"
#include "KnxDevice/TimeUtils.h"
#elif KNX_LIBRARY == 2
#include "KnxTpUart/KnxTpUart.h"
#endif

#if KNX_LIBRARY == 1
/* Global KNX definitions:
 *
 * index | group address | 		data type    	| flags
 *     0 |	    1/1/0    | 1.001 B1 DPT_Switch	| C W U I
 */
KnxComObject KnxDevice::_comObjectsList[] =
{
	/* Index 0 : */KnxComObject(G_ADDR( 1,1,0 ), KNX_DPT_1_001, KNX_COM_OBJ_C_W_U_I_INDICATOR )
};
const uint8_t KnxDevice::_comObjectsNb = sizeof(_comObjectsList) / sizeof(KnxComObject); // do no change this code

void knxEvents( uint8_t index )
{
	switch( index ) {
		case 0: // object index 0 has been updated
			// LED to reflect Boolean value of object
			Board_LED_Set( 1, Knx.read( 0 ) );
			break;

		default:
			break;
	}
}
#elif KNX_LIBRARY == 2
void knx_task( KnxTpUart &knx )
{
	KnxTpUartSerialEventType eType = knx.serialEvent();

	//Evaluation of the received telegram -> only KNX telegrams are accepted
	if( eType == KNX_TELEGRAM ) {
		KnxTelegram* telegram = knx.getReceivedTelegram();
		uint16_t targetGA = GROUP_ADDR( telegram->getTargetMainGroup(), telegram->getTargetMiddleGroup(), telegram->getTargetSubGroup() );

		if( targetGA == GROUP_ADDR( 1, 0, 0 ) || targetGA == GROUP_ADDR( 1, 1, 0 ) ) {
			KnxCommandType command = telegram->getCommand();
			if( command == KNX_COMMAND_ANSWER || command == KNX_COMMAND_WRITE )	// Since we're not explicitly requesting value, it's not going to be KNX_COMMAND_ANSWER
					{
				bool value = telegram->getBool();
				Board_LED_Set( 1, value );
				if( value ) {
					knx.groupWriteBool( GROUP_ADDR( 1,0,1 ), true );
				}
			}
		}
	}
}
#endif

int main( void )
{
	SystemCoreClockUpdate();
	Board_Init();
	Board_LED_Set( 0, true );

#if KNX_LIBRARY == 1
	// Start 1 MHz counter
	TimeUtils::init();
#endif
	SysTick_init( 1000 );	// Initialize systick counting for milliseconds

	// Initialize ring-buffer serial port
	HardwareSerial	*serial;
#if defined( BOARD_NXP_LPCXPRESSO_11U68_JWJ )
	serial = new Uart1RingBufferSerial();
	SysTick_delay( 500 );
#elif defined( BOARD_NXP_LPCXPRESSO_11U68 )
	serial = new Uart0RingBufferSerial();
#endif
	Board_LED_Set( 0, false );

	// Initialize KNX
#if KNX_LIBRARY == 1
	Knx.begin( *serial, P_ADDR( 1,1,100 ));
	SysTick_delay( 100 );
#elif KNX_LIBRARY == 2
	KnxTpUart knx( &serial, 1, 1, 100 );

	serial.begin( 19200, SERIAL_8E1 );
	knx.uartReset();

	knx.addListenGroupAddress( GROUP_ADDR( 1, 0, 0 ) );	// Switch
	knx.addListenGroupAddress( GROUP_ADDR( 1, 1, 0 ) ); // Status
#elif KNX_LIBRARY == 3
	serial->begin( 19200, SERIAL_8E1 );

	SysTick_delay( 100 );

	// Read 6 bytes
	uint8_t buffer[ 32 ];
	uint8_t flag = 0;
	int bytesRead = 0;

	const char message[] = "Hejsa, Jens";
	serial->write( (uint8_t*)message, sizeof( message ) - 1 );


	while( bytesRead < 9 ) {
		if( serial->available() > 0 )
		{
			buffer[ bytesRead++ ] = serial->read();
		}

		if( bytesRead == 6 ) {
			flag++;
			// Header received. Expecting BC 11 01 08 00 E1
		}
		if( bytesRead == 9 ) {
			// Full packet received. Expecting BC 11 01 08 00 E1 00 80 3A
			flag++;
		}
	}

	// Buffer full: end execution
#endif

	while( true ) {
#if KNX_LIBRARY == 1
		Knx.task();
#elif KNX_LIBRARY == 2
		knx_task( knx );
#endif
	}

	return 0;
}
